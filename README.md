# Project Title

Minecraft Electric Age Project
-------------------------------
This is a project developped during SInf lesson (S4 Hes-so Valais/Wallis)
This project use Minecraft Electric age mode, a web page interface and a database.
The connection between all these components are controlled by our program (MinecraftController)
We have develop the modbus protocole that connect the minecraft mode with the MinecraftController.
We use HTTPs protocole to connect database and the web page to the MinecraftController.

## Getting Started

1. Clone the gitlab project
2. Load the GrafanaDashSIn37.json file in your database viewer
3. Start Minecraft and enter in the SIn 2021 world
4. Start our JAR file with your Database path connection in parameter
5. Open the WebClientSIn37/index.html
6. See how it works

### Prerequisites

All you need to instal is here : https://cyberlearn.hes-so.ch/course/view.php?id=16350

- Minecraft Electrical Age mode
- SIn world 2021
- GIT
- IntelliJ

## Versioning

V0.1

## Authors

Rudaz Patrice - Initial work - (https://gitlab.com/patrudaz)
Zufferey Samuel - Core, Database, Web - (https://gitlab.com/SamZuff)
Crettenand Romain - Modbus, Field, Core -(https://gitlab.com/romcre)