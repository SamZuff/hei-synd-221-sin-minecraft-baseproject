package ch.hevs.isi.core;

import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.smart.SmartController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
/**
 * Class DataPoint
 * A Datapoint is a point that has a label as a name and a boolean value
 * That says if its an output or an input
 */
public class DataPoint {
    private static Map<String, DataPoint> dataPointMap = new HashMap<>();
    private String label;
    private boolean isOutput;

    // Constructor
    protected DataPoint(String label, boolean isOutput){
        this.label = label;
        this.isOutput = isOutput;
        dataPointMap.put(label, this);
    }

    /**
     * Function that return a datapoint from the name of the label
     * @param label
     * @return  return a DataPoint
     */
    public static DataPoint getDataPointFromLabel(String label){
        return dataPointMap.get(label);
    }

    /**
     * Function to get the label
     * @return the label of the data point
     */
    public String getLabel(){ return label; }

    /**
     * Function to get if the data point in an output
     * @return a boolean value
     */
    public boolean isOutput(){
        return isOutput;
    }


    public static void main(String[] args) {
        BooleanDataPoint bdp1 = new BooleanDataPoint("REMOTE_SOLAR_SW", true);
        bdp1.setValue(false);
        FloatDataPoint fdp1 = new FloatDataPoint("BATT_CHRG_FLOAT", true);
        float value = 0f;

        while(true){
            fdp1.setValue(value);
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                System.out.println("Thread is interrupted...");
            }
            value+=0.01f;
            if(value > 1f){
                value = 0f;
            }
        }
    }
}
