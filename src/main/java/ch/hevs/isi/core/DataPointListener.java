package ch.hevs.isi.core;

/**
 * Interface for our singleton connector
 */
public interface DataPointListener {
    abstract public void onNewValue(FloatDataPoint fdp);
    abstract public void onNewValue(BooleanDataPoint bdp);
}
