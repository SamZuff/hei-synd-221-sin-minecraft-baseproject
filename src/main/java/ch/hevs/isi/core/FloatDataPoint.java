package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.smart.SmartController;
import ch.hevs.isi.web.WebConnector;

public class FloatDataPoint extends DataPoint{
    private float value;
    public FloatDataPoint(String label, boolean isOutput){
        super(label, isOutput);
    }

    /**
     * Method that set the value of the boolean
     * @param value value to set
     * put the new value on the DatabaseConnector and on the WebConnector
     * if the value is an output then it is also put on the field connector
     */
    public void setValue(float value) {
        this.value = value;
        DatabaseConnector dbc = DatabaseConnector.getInstance();
        dbc.onNewValue(this);
        WebConnector wbc = WebConnector.getInstance();
        wbc.onNewValue(this);
        if(isOutput()){
            FieldConnector fbc = FieldConnector.getInstance();
            fbc.onNewValue(this);
        }
    }

    /**
     * Method to get the float value
     * @return the float value
     */
    public float getValue(){
        return value;
    }
}
