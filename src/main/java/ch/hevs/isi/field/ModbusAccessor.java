package ch.hevs.isi.field;

import ch.hevs.isi.field.Frame.FrameMaker;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Class modbus Accessor
 * inherit from socket
 * Access the game and interact with it
 */
public class ModbusAccessor extends Socket {
    // Private attribute
    private static ModbusAccessor x_c = null;
    FrameMaker fm = new FrameMaker();
    Transaction tr = new Transaction();
    OutputStream out;
    InputStream in;
    byte[] bArray;
    byte[] bReceive;
    public static final int TRUE_VALUE = 0xff00;


    // Private constructor
    private ModbusAccessor() throws IOException {
        super("localhost",1502);
        out = getOutputStream();
        in = getInputStream();
    }

    // The static method getInstance() returns a reference to the singleton.
// It creates the single X_Connector object if it does not exist.
    public static ModbusAccessor getInstance() throws IOException {
        if (x_c == null) {
            x_c = new ModbusAccessor();
        }
        return x_c;
    }

    /**
     * Read a float value in function of the address sended
     * @param regAddress     register address to access
     * @return  Return a float value
     */
    public float readFloat(int regAddress){
        float fValue;
        bArray = fm.getFloatFrame(3,regAddress,2f,0);
        try {
            bReceive = tr.process(bArray,out,in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fValue = fm.getFloatFromFrame(bReceive);
        System.out.println("float Value  " + fValue);
        return fValue;
    }

    /**
     * Read a boolean value in function of the address
     * @param regAddress    Register address to access
     * @return  Return the boolean value
     */
    public boolean readBoolean(int regAddress){
        boolean value;
        bArray = fm.getBoolFrame(1, regAddress,1);
        try {
            bReceive = tr.process(bArray,out,in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        value = fm.getBoolFromFrame(bReceive);
        return value;
    }

    /**
     * Write a float value on an address of the game
     * @param regAddress    Register address to access
     * @param floatValue    Float Value to write
     * @return return the boolean value that say if the message has been correctly send
     */
    public boolean writeFloat(int regAddress,float floatValue){
        //send the value to the frame
        bArray = fm.getFloatFrame(0x10,regAddress,floatValue,1);
        try {
            bReceive = tr.process(bArray,out,in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean result = fm.getEchoFWrite(bArray,bReceive);
        return result;
    }

    /**
     * Write a boolean value on an address of the game
     * @param regAddress    Register address to access
     * @param boolValue     Boolean Value to write
     * @return return the boolean value that say if the message has been correctly send
     */
    public boolean writeBoolean(int regAddress, boolean boolValue){
        //send the boolean to the frame
        if(boolValue == true)
        {
            bArray = fm.getBoolFrame(5,regAddress,TRUE_VALUE);
        }
        else{
            bArray = fm.getBoolFrame(5,regAddress,0);
        }
        try {
            bReceive = tr.process(bArray,out,in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean result = fm.getEchoB(bArray,bReceive);
        return result;
    }

    public static void main(String[] args) throws IOException {
        ModbusAccessor md = ModbusAccessor.getInstance();
        int value = 3;
        while(value > 1){

/*
           // System.out.println(fValue*1500);
           //md.readFloat(61);
           // valueF = valueF * 1000;
           // md.writeBoolean(401,true);
            boolean bvalue = md.writeBoolean(405,false);
            System.out.println("Write bool : "+bvalue);
            //bvalue = md.writeFloat(209,0f);
            System.out.println("Write float " +bvalue);
            bvalue = md.readBoolean(609);
            System.out.println("Read Bool " +bvalue);
            float fvalue = md.readFloat(61);
            System.out.println("Read float " +fvalue);
            //System.out.println(result);
            value--;*/
        }
    }

}
