package ch.hevs.isi.field.Frame;

import java.nio.ByteBuffer;

/**
 * Class FrameMaker makes the frame in a byte Array in order to send the
 * right message
 */
public class FrameMaker {
    ByteBuffer bb;
    byte[] bArray;
    ByteBuffer bReceive;
    public FrameMaker() {

    }

    /**
     * Function that return a frame for a float value
     * @param messageType   variable that tell de method modbus
     * @param regAddress    Address of the register we want to access
     * @param reg           Quantity of register in the frame
     * @param rOrW          Variable read or write
     * @return Return the frame as an array of Byte
     */
    public byte[] getFloatFrame(int messageType, int regAddress, float reg,int rOrW){
        PDU thePdu;

        if(rOrW == 1){
            thePdu = new PDU(messageType,regAddress,2,4,reg);
        }
        else{
            int val = (int)reg;
            thePdu = new PDU(messageType,regAddress,val);
        }
        int size = thePdu.getByteBuffer(rOrW).capacity();
        MBAPH theMbaph = new MBAPH(size);
        bb = ByteBuffer.allocate(thePdu.getByteBuffer(rOrW).capacity()+theMbaph.getByteBuffer().capacity());
        bb.put(theMbaph.getByteBuffer()).put(thePdu.getByteBuffer(rOrW));
        bArray = bb.array();
        return bArray;
    }

    /**
     * Method to get the boolean frame
     * @param messageType Variable that define the type of the message
     * @param regAddress  Variable that tell which register adress we need
     * @param reg         Tells the register
     * @return an array of byte
     */
    public byte[] getBoolFrame(int messageType, int regAddress,int reg){
        PDU thePdu = new PDU(messageType,regAddress,reg);
        int size = thePdu.getByteBuffer(0).capacity();
        MBAPH theMbaph = new MBAPH(size);
        bb = ByteBuffer.allocate(thePdu.getByteBuffer(0).capacity()+theMbaph.getByteBuffer().capacity());
        bb.put(theMbaph.getByteBuffer()).put(thePdu.getByteBuffer(0));
        bArray = bb.array();
        System.out.print("Message send:");
        for(int i = 0;i< bArray.length;i++) {
            System.out.print(bArray[i] + " ");
        }
        System.out.println("");
        return bArray;
    }

    /**
     * Method that decode the float frame received from minecraft in
     * order to get the float value of the message
     * @param bArray    array received from the Input stream
     * @return return a float value
     */
    public float getFloatFromFrame(byte[] bArray){
        bb = ByteBuffer.wrap(bArray);
        short ti = bb.getShort(0);
        short pi = bb.getShort(2);
        short len = bb.getShort(4);
        byte ui = bb.get(6);
        byte fc = bb.get(7);
        byte n = bb.get(8);
        float value = bb.getFloat(9);
        System.out.println("Function code " +fc);
        return value;
    }

    /**
     * Method that decode the boolean frame received from minecraft in
     * order to get the boolean value of the message
     * @param bArray    array received from the Input stream
     * @return return a boolean
     */
    public boolean getBoolFromFrame(byte[] bArray){
        bb = ByteBuffer.wrap(bArray);
        boolean x;
        short ti = bb.getShort(0);
        short pi = bb.getShort(2);
        short len = bb.getShort(4);
        byte ui = bb.get(6);
        byte fc = bb.get(7);
        byte n = bb.get(8);
        byte value = bb.get(9);
        if(value == 1){
            x = true;
        }
        else
        {
            x = false;
        }
        System.out.println("Function code " +fc);

        return x;
    }

    /**
     * Method that check if there is a problem in the answer of the boolean frame
     * @param bArraySend Array sended to the stream
     * @param bArrayReceive Array received from the stream
     * @return a boolean value
     */
    public boolean getEchoB(byte[] bArraySend,byte[] bArrayReceive){
        bb = ByteBuffer.wrap(bArraySend);
        boolean x;
        short value = bb.getShort(9);
        bReceive = ByteBuffer.wrap(bArrayReceive);
        short valuer = bReceive.getShort(9);
        if (value == valuer){
            return true;
        }
        else{
            return false;
        }

    }
    /**
     * Method that check if there is a problem in the answer of the float frame
     * @param bArraySend Array sended to the stream
     * @param bArrayReceive Array received from the stream
     * @return a boolean value
     *
     */
    public boolean getEchoF(byte[] bArraySend,byte[] bArrayReceive) {
        bb = ByteBuffer.wrap(bArraySend);
        bReceive = ByteBuffer.wrap(bArrayReceive);
        boolean x;
        float value = bb.getFloat(9);
        float valuer = bReceive.getFloat(9);
        if (value == valuer) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method that checked if writing a float has been succesful or not
     * @param bArraySend Array sended to the stream
     * @param bArrayReceive Array received from the stream
     * @return Return true if the writing has been correctly send
     */
    public boolean getEchoFWrite(byte[] bArraySend,byte[] bArrayReceive) {
        bb = ByteBuffer.wrap(bArraySend);
        bReceive = ByteBuffer.wrap(bArrayReceive);
        boolean x;
        short value = bb.getShort(10);
        short valuer = bReceive.getShort(10);
        if (value == valuer) {
            return true;
        } else {
            return false;
        }
    }
}

