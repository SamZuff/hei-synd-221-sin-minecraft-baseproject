package ch.hevs.isi.field.Frame;

import java.nio.ByteBuffer;
/**
 * Class PDU that take care of the pdu part of the frame
 */
public class PDU {
    short regAddress;
    byte messageType;
    short regQuantity;
    byte byteCount;
    float regValue;
    ByteBuffer bb;


    public PDU(int messageType,int regAddress,int regQuantity){

        this.messageType = (byte)messageType;
        this.regAddress = (short)regAddress;
        this.regQuantity = (short)regQuantity;
        bb = ByteBuffer.allocate(5);
    }
    public PDU(int messageType,int regAddress,int regQuantity,int byteCount,float regValue){

        this.messageType = (byte)messageType;
        this.regAddress = (short)regAddress;
        this.regQuantity = (short)regQuantity;
        this.byteCount = (byte)byteCount;
        this.regValue = regValue;
        bb = ByteBuffer.allocate(10);
    }
    /**
     * Method that get the pdu byte buffer for the frame
     * @param rOrW Param that modify the type of the message if we
     *             want to write or read a value (not the same size)
     * @return  Return a byteBuffer of the PDU correctly set
     */
    public ByteBuffer getByteBuffer(int rOrW){
        if(rOrW == 0) {
            bb.put(0, (byte) messageType);
            bb.putShort(1, regAddress);
            bb.putShort(3, regQuantity);
        }
        else
        {
            bb.put(0,messageType);
            bb.putShort(1,regAddress);
            bb.putShort(3,regQuantity);
            bb.put(5,byteCount);
            bb.putFloat(6,regValue);
        }
        return bb;
    }
}
