package ch.hevs.isi.field.Frame;

import java.nio.ByteBuffer;

/**
 * Class with a unique method that return a byteBuffer for the MBAPH
 */
public class MBAPH {
    short transId = 1;
    short protId = 0;
    short len;
    byte unitIden = 1;
    ByteBuffer bb;

    public MBAPH(int len){
        this.len = (short)(len+1);
    }

    /**
     * Function that return a byteBuffer of the MBAPH correctly set
     * @return
     */
    public ByteBuffer getByteBuffer(){
        bb = ByteBuffer.allocate(7);
        bb.putShort(0,transId);
        bb.putShort(2,protId);
        bb.putShort(4,len);
        bb.put(6,unitIden);
        return bb;
    }



}
