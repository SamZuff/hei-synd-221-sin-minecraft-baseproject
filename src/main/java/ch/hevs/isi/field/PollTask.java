package ch.hevs.isi.field;

import ch.hevs.isi.smart.SmartController;

import java.util.TimerTask;

/**
 * Class of polling method, update the field method at every thread
 */
public class PollTask extends TimerTask {

    @Override
    public void run() {
        FieldConnector.poll();

    }
}
