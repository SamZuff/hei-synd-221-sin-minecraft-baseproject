package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Class Transaction Send the message to the OutputStream and read the answer
 */
public class Transaction {
    /**
     * Class that process the send message to the stream
     * @param bArray    The frame to send
     * @param out       The outputstream
     * @param in        The inputstream
     * @return          return the answer of modbus
     * @throws IOException
     */
    public byte[] process(byte[] bArray, OutputStream out, InputStream in) throws IOException {
        byte[] read;
        Utility.sendBytes(out,bArray,0,bArray.length);
        while((read = Utility.readBytes(in)) == null);
        return read;
    }
}
