package ch.hevs.isi.field;

import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;

/**
 * Specialization of a register in order to add float registers
 */
public class FloatRegister extends Register {

    ModbusAccessor ma;
    FloatDataPoint fdp;
    int address;
    int range;
    int offset;

    {
        try {
            ma = ModbusAccessor.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor of a float register
     * @param address   Variable of the address from the csv file
     * @param isOutput  Boolean value to tell if the register is an output or not
     * @param label     Name of the register
     * @param offset    Add an offset if there is an offset in the database
     * @param range     Variable that indicate the range of the value
     */
    FloatRegister(int address,String label,boolean isOutput,int range,int offset){
        fdp = new FloatDataPoint(label,isOutput);
        this.address  = address;
        this.range = range;
        this.offset = offset;
    }
    /**
     * Method that read the floatRegister and set the new value to the datapoint
     */
    @Override
    void read(){
        float returnedValue;
        returnedValue = ma.readFloat(address);
        returnedValue = (returnedValue*range) + offset;
        fdp.setValue(returnedValue);

    }

    /**
     * Method that write the float register on the modbus
     */
    @Override
    void write(){
        float value = fdp.getValue();
        value = (value-offset)/range;
        ma.writeFloat(address,value);
    }
}
