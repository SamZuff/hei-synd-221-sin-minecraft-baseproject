package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;

/**
 * Specialization of a register in order to add float registers
 */
public class BooleanRegister extends Register{

    BooleanDataPoint bdp;
    int address;

    ModbusAccessor ma;
    {
        try {
            ma = ModbusAccessor.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Constructor of a boolean register
     * @param address   Variable of the address from the csv file
     * @param isOutput  Boolean value to tell if the register is an output or not
     * @param label     Name of the register
     */
    BooleanRegister(int address,String label,boolean isOutput){
        bdp = new BooleanDataPoint(label,isOutput);
        this.address  = address;
    }

    /**
     * Method that read the floatRegister and set the new value in the boolean
     * Datapoint
     */
    @Override
    void read(){
        boolean returnedValue;
        returnedValue = ma.readBoolean(address);
        bdp.setValue(returnedValue);
    }

    /**
     * Method that write the boolean register on the modbus
     */
    @Override
    void write(){
        boolean value = bdp.getValue();
        System.out.println("Check bool value :" + value);
        ma.writeBoolean(address,value);
    }
}
