package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.smart.SmartController;
import ch.hevs.isi.utils.Utility;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;


import static java.lang.String.valueOf;

/**
 * Class Field Connector
 * @see ch.hevs.isi.core.DataPointListener
 */
public class FieldConnector implements DataPointListener {
    // Private attribute
    private static FieldConnector x_c = null;
    private static HashMap<String,Register> registerMap = new HashMap<>();
    static Timer pollTimer = new Timer();
    // Private constructor
    private FieldConnector() {};
    // The static method getInstance() returns a reference to the singleton.
    // It creates the single X_Connector object if it does not exist.
    public static FieldConnector getInstance() {
        if (x_c == null) { x_c = new FieldConnector();
            loadRegister("Data.csv");
            pollTimer.scheduleAtFixedRate(new PollTask(),0,4000);
        }
        return x_c;
    }

    /**
     * Load register from the CSV file (values separated by ';')
     * @param path      The path to the file
     */
    private static void loadRegister(String path){
        //Matching column index
        final int labelC = 0, typeC = 1, textC = 2,yC = 3, outputC = 4, addressC = 5, rangeC = 6, offsetC = 7;
        final String splitSymbol = ";";

        try {
            String label;
            String type;
            boolean isOutput;
            int address, range, offset;

            String line;
            String[] splited;
            boolean firstLine = true;

            //Create a new reader
            BufferedReader br = Utility.fileParser(null, path); // new BufferedReader(new FileReader(path));

            //Load register as until end of file
            while ((line = br.readLine()) != null) {
                if (firstLine) {  //Skip if first line
                    firstLine = false;
                    continue;
                }

                //Split line in an array of string
                splited = line.split(splitSymbol);

                //Retrieve label and type
                label = splited[labelC];
                type = splited[typeC];

                //Is the register an output or an input?
                if (splited[outputC].equals("Y"))
                    isOutput = true;
                else
                    isOutput = false;

                //Read Modbus address, its range and offset values
                address = Integer.valueOf(splited[addressC]);
                range = Integer.valueOf(splited[rangeC]);
                offset = Integer.valueOf(splited[offsetC]);

                //Create corresponding register
                if (type.equals("[ON|OFF]"))
                    registerMap.put(label, new BooleanRegister(address, label, isOutput));
                else
                    registerMap.put(label, new FloatRegister(address, label, isOutput, range, offset));

                System.out.println("Loading register " + label);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done !\n");
    }

    /**
     * Push to the field that contains all the registers
     * @param label     The location where to push on the field
     * @param value     The value to push on the field
     */
    public void pushToField(String label, String value){
        System.out.println("Field updated : " + label + ", " + value);
    }

    /**
     * Return a register in function of the label
     * @param label     The label of the register needed
     */
    public Register getRegisterFromLabel(String label){
        return registerMap.get(label);
    }

    /**
     * Read all the register in the Hashmap
     */
    public static void poll(){
        registerMap.forEach((label,register)-> register.read());
    }

    /**
     * New boolean value this function push to the field the new value
     * @param bdp     Boolean Data point
     */
    @Override
    public void onNewValue(BooleanDataPoint bdp) {
        pushToField(bdp.getLabel(), valueOf(bdp.getValue()));
        Register reg = getRegisterFromLabel(bdp.getLabel());
        reg.write();
    }

    /**
     * New Float value this function push to the field the new value
     * @param fdp     Float Data point
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {

        pushToField(fdp.getLabel(), valueOf(fdp.getValue()));
        Register reg = getRegisterFromLabel(fdp.getLabel());
        reg.write();
    }


    public static void main(String[] args) {
        FieldConnector.getInstance();
        SmartController.getInstance();

    }
}
