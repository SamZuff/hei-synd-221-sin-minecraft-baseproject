package ch.hevs.isi.database;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

import static java.lang.String.valueOf;

// Database : SIn37
// Username : SIn37
// Password : 7d9f8be6d7572cefbc658e6b41e9f302

/**
 * Class DatabaseConnector
 * This class send new values to our influxDB database.
 */
public class DatabaseConnector implements DataPointListener {
        /**
         * Private attribute
         */
        private static DatabaseConnector x_c = null;

        /**
         * Private constructor
         */
        private DatabaseConnector() {};

        /**
         * The static method getInstance() returns a reference to the singleton.
         * It creates the single X_Connector object if it does not exist.
         * @return A reference to the singleton
         */
        public static DatabaseConnector getInstance() {
            if (x_c == null) { x_c = new DatabaseConnector(); }
            return x_c;
        }

        /**
         * This function send the new values to our influxDB database
         * @param label is the name of the changed register
         * @param value is the new value of the register
         */
        public void pushToDB(String label, String value) {
                try {
                        URL url = new URL("https://influx.sdi.hevs.ch/write?db=SIn37");
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        String userpass = "SIn37:" + Utility.md5sum("SIn37");
                        String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
                        connection.setRequestProperty("Authorization", "Basic " + encoding);
                        connection.setRequestProperty("Content-Type", "binary/octet-stream");
                        connection.setRequestMethod("POST");
                        connection.setDoOutput(true);

                        String msg = label + " value=" + value;
//                        Utility.writeLine(connection.getOutputStream(), msg.getBytes());
                        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                        writer.write(msg);
                        writer.flush();
                        int responseCode = connection.getResponseCode();
                        System.out.println("Response code : " + responseCode);
//                        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                        if(responseCode != 204){
                                System.out.println("pushToDB() ERROR: responseCode: " + responseCode);
//                                while((in.readLine() != null)){
//                                while(Utility.readLine(connection.getInputStream()) != null){
//                                        // Here we will process data
//                                }
                        }
                        System.out.println("Database updated : " + label + ", " + value);
                        connection.disconnect();

                } catch (MalformedURLException e) {
                        e.printStackTrace();
                } catch (IOException e) {
                        e.printStackTrace();
                }
        }

        /**
         * Override the function of DataPointListener
         * @param fdp is the FloatDataPoint that has changed
         */
        @Override
        public void onNewValue(FloatDataPoint fdp) {
                pushToDB(fdp.getLabel(), valueOf(fdp.getValue()));
        }

        /**
         * Override the function of DataPointListener
         * @param bdp is the BooleanDataPoint that has changed
         */
        @Override
        public void onNewValue(BooleanDataPoint bdp) {
                pushToDB(bdp.getLabel(), valueOf(bdp.getValue()));
        }
}