package ch.hevs.isi.web;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;
import java.util.List;

import static java.lang.String.valueOf;

public class WebConnector extends WebSocketServer implements DataPointListener {
    /**
     * Private attribute
     */
    private static WebConnector x_c = null;

    /**
     * Private constructor
     */
    private WebConnector() {
        super(new InetSocketAddress(8888));
        start();
    }

    /**
     * Override the function of WSS
     * @param webSocket is the connection that has opened
     * @param clientHandshake
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        System.out.println("Client is connected");
    }

    /**
     * Override the function of WSS
     * Print a message when a connection has closed
     * @param webSocket is the connection that has closed
     * @param i The codes can be looked up by a CloseFrame
     * @param s additional information string
     * @param b Returns whether or not the closing of the connection was initiated by the remote host
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        System.out.println("Client is disconnected");
    }

    /**
     * Override the function of WSS
     * Decode the message received and update the concerned register
     * @param webSocket is the source of the message received
     * @param s is the message
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        System.out.println("A message is arrived : " + s);
        String[] sSplit = s.split("=");
        String label = sSplit[0];
        String value = sSplit[1];
        DataPoint dp = DataPoint.getDataPointFromLabel(label);

        if(dp instanceof BooleanDataPoint){
            boolean newVal;
            if(value.equals("true")){
                newVal = true;
            }
            else{
                newVal = false;
            }
            System.out.println("NewVal : " +newVal);
            ((BooleanDataPoint) dp).setValue(newVal);
        }
        if(dp instanceof  FloatDataPoint){
            ((FloatDataPoint) dp).setValue(Float.parseFloat(value));
        }
    }

    /**
     * Override the function of WSS
     * Print a message error when an error has occured
     * @param webSocket is the connection that has failed
     * @param e is the exception
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {
        System.out.println("WebServerSocket error : ");
        e.printStackTrace();
    }

    /**
     * Override the function of WSS
     * Print a message that the WSS has started
     */
    @Override
    public void onStart() {
        System.out.println("WebServerSocket is started !");
    }

    /**
     * The static method getInstance() returns a reference to the singleton.
     * It creates the single X_Connector object if it does not exist.
     */
    public static WebConnector getInstance() {
        if (x_c == null) { x_c = new WebConnector(); }
        return x_c;
    }

    /**
     * Send new values to web pages
     * @param label is the name of the changed register
     * @param value is the new value of the register
     */
    public void pushToWebPages(String label, String value){
        broadcast(label + "=" + value);
        System.out.println("Web pages updated : " + label + ", " + value);
    }

    /**
     * Override the function of DataPointListener
     * @param fdp
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        pushToWebPages(fdp.getLabel(), valueOf(fdp.getValue()));
    }

    /**
     * Override the function of DataPointListener
     * @param bdp
     */
    @Override
    public void onNewValue(BooleanDataPoint bdp) {
        pushToWebPages(bdp.getLabel(), valueOf(bdp.getValue()));
    }
}
