package ch.hevs.isi.smart;

import java.util.TimerTask;

/**
 * Class that is a thread for the timer of the SmartController
 * inherit from TimerTask
 */
public class PollTaskSC extends TimerTask {

    @Override
    public void run() {
        SmartController.controlling();

    }
}
