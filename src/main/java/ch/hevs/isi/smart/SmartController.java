package ch.hevs.isi.smart;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Timer;


/**
 * Class SmartController make that the game is energy sufficient
 * without needing to use more than the coal needed
 */
public class SmartController {
    private static SmartController sC = null;
    static Timer pollTimer = new Timer();

    public static SmartController getInstance() {
        if (sC == null) {
            sC = new SmartController();
            pollTimer.scheduleAtFixedRate(new PollTaskSC(),2000,4000);
        }
        return sC;
    }

    /**
     * Method to control the minecraft world
     */
    public static void controlling(){
        float clock = ((FloatDataPoint) DataPoint.getDataPointFromLabel("CLOCK_FLOAT1")).getValue();
        FloatDataPoint coalRemote = ((FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_COAL_SP2"));
        FloatDataPoint factoryRemote = ((FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP2"));
        BooleanDataPoint solarRemote = ((BooleanDataPoint) DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW1"));
        BooleanDataPoint windRemote = ((BooleanDataPoint) DataPoint.getDataPointFromLabel("REMOTE_WIND_SW1"));

        float battCharge = ((FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT1")).getValue();


            //work with the day and night cycle
            System.out.println("clock value : " + clock);
            if (clock > 0.29 && clock < 0.71) {
                battCharge *= 100;
                // Control Coal Factory
                if(battCharge < 30){
                    coalRemote.setValue(1f);
                }
                else if(battCharge < 50)
                {
                    coalRemote.setValue(0.5f);
                }
                else
                {
                    coalRemote.setValue(0f);
                }

                // Control Factory
                if (battCharge < 80)
                {
                    factoryRemote.setValue(0f);
                }
                else
                {
                    factoryRemote.setValue(0.6f);
                }

                // Control Solar and wind producers
                if(battCharge < 95)
                {
                    solarRemote.setValue(true);
                    windRemote.setValue(true);
                }
                else {
                    solarRemote.setValue(false);
                    windRemote.setValue(false);
                }


            } else {
                battCharge *= 100;


                // Control Coal Factory
                if(battCharge < 30){
                    coalRemote.setValue(1f);
                }
                else if(battCharge < 50)
                {
                    coalRemote.setValue(0.5f);
                }
                else
                {
                    coalRemote.setValue(0f);
                }

                // Factory shutdown during night
                factoryRemote.setValue(0f);


                // Control Solar and wind producers
                if(battCharge < 95)
                {
                    solarRemote.setValue(true);
                    windRemote.setValue(true);
                }
                else {
                    solarRemote.setValue(false);
                    windRemote.setValue(false);
                }
            }


    }
}
